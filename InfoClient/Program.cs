﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CNA_Tema1;
using Grpc.Net.Client;

namespace InfoClient
{
	class Program
	{
		static async Task Main(string[] args)
		{
			using GrpcChannel channel = GrpcChannel.ForAddress("https://localhost:5001");
			InfoManager.InfoManagerClient client = new InfoManager.InfoManagerClient(channel);

			Console.Write("Full Name: ");
			string name = Console.ReadLine()?.Trim();

			Console.Write("CNP: ");
			string cnp = Console.ReadLine()?.Trim();

			if (!ValidateData(name, cnp))
			{
				Console.WriteLine("Data input is invalid. The program is stopping...");
				return;
			}

			InfoReply response = await client.SendInfoAsync(new InfoRequest() {Name = name, Cnp = cnp});
			Console.WriteLine($"Name: {response.Name} | Age: {response.Age} | Gender: {response.Gender}");
			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		private static bool ValidateData(string name, string cnp)
		{
			if (!name.All(c => char.IsLetter(c) || c == ' '))
				return false;

			if (cnp.Length != 13)
				return false;

			if (!cnp.All(char.IsDigit))
				return false;

			return true;
		}
	}
}