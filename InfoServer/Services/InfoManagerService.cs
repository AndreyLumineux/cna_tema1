using System;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace CNA_Tema1.Services
{
	public class InfoManagerService : InfoManager.InfoManagerBase
	{
		private readonly ILogger<InfoManagerService> logger;

		public InfoManagerService(ILogger<InfoManagerService> logger)
		{
			this.logger = logger;
		}

		public override Task<InfoReply> SendInfo(InfoRequest request, ServerCallContext context)
		{
			string gender;
			DateTime birthDate;

			int year = Convert.ToInt32(request.Cnp.Substring(1, 2));
			int month = Convert.ToInt32(request.Cnp.Substring(3, 2));
			int day = Convert.ToInt32(request.Cnp.Substring(5, 2));

			switch (request.Cnp[0])
			{
				case '1':
				{
					gender = "Male";
					year += 1900;
					break;
				}
				case '2':
				{
					gender = "Female";
					year += 1900;
					break;
				}
				case '3':
				{
					gender = "Male";
					year += 1800;
					break;
				}
				case '4':
				{
					gender = "Female";
					year += 1800;
					break;
				}
				case '5':
				{
					gender = "Male";
					year += 2000;
					break;
				}
				case '6':
				{
					gender = "Female";
					year += 2000;
					break;
				}
				default:
				{
					gender = "ERROR";
					birthDate = DateTime.Today;
					break;
				}
			}

			birthDate = new DateTime(year, month, day);
			TimeSpan timePassedSinceBirth = DateTime.Today.Subtract(birthDate);
			DateTime dayOneDateTime = new DateTime(1, 1, 1);

			int age = (dayOneDateTime + timePassedSinceBirth).Year - 1;

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine($"Replying to {request.Name}. Gender: {gender}. Age: {age}.");
			Console.ForegroundColor = ConsoleColor.Gray;

			return Task.FromResult(new InfoReply
			{
				Name = request.Name,
				Gender = gender,
				Age = age
			});
		}
	}
}